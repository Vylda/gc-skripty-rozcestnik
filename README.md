# GC skripty rozcestník

Odkazy na zajímavé stránky a skripty pro geocaching

## Webovky

### Project GC
- [Splněné challenge](https://project-gc.com/Profile/SignedChallenges)
- [Vilrtuální GPSka](https://project-gc.com/User/VirtualGPS)
- [Porovnání mapy](https://project-gc.com/Tools/MapCompare)
- [Záchrana TB](https://project-gc.com/Tools/TBRescue)

### dCode.fr
[Seznam všech nástrojů](https://www.dcode.fr/tools-list)

### Geocaching toolbox
- [Nekompletní souřadnice](https://www.geocachingtoolbox.com/index.php?lang=en&page=incompleteCoordinates)
- [Projekce souřadnic](https://www.geocachingtoolbox.com/index.php?lang=en&page=coordinateProjection)
- [Analýza textu](https://www.geocachingtoolbox.com/index.php?lang=en&page=textAnalysis)
- [Kódovací tabulky](https://www.geocachingtoolbox.com/index.php?lang=en&page=codeTables&id=overview)

### [GCgpx.cz](http://gcgpx.cz)

## Skripty
### Spouštěče
#### Tamper Monkey

- [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
- [Firefox](https://addons.mozilla.org/cs/firefox/addon/tampermonkey/)

#### Grease Monkey

- [Firefox](https://addons.mozilla.org/cs/firefox/addon/greasemonkey/)

### Skripty
#### Geocaching Map Enhancements
- [originál](https://openuserjs.org/scripts/JRI/Geocaching_Map_Enhancements)
- [můj fork](http://gcgpx.cz/gme/)

##### Mapy

**Pozor, Mapy.cz v současné době (konec ledna 2023) neposkytují mapové dlaždice, proto jsou defaultně vypnuté!**

```json
[
  {"alt":"Mapy.cz - Základní","tileUrl":"http://mapserver.mapy.cz/base-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - Dopravní","tileUrl":"https://mapserver.mapy.cz/base-m/{z}-{x}-{y}?s=0.2&dm=Luminosity","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA","ignore":false},
  {"alt":"Mapy.cz - Letní","tileUrl":"https://mapserver.mapy.cz/turist_aquatic-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA","ignore":false},
  {"alt":"Mapy.cz - Zimní","tileUrl":"https://mapserver.mapy.cz/wturist_winter-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA","ignore":false},
  {"alt":"Mapy.cz - Zeměpisná","tileUrl":"https://mapserver.mapy.cz/zemepis-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA","ignore":false},
  {"alt":"Mapy.cz - Letecká","tileUrl":"https://mapserver.mapy.cz/ophoto-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='//eox.at/' target='_blank'>EOX IT Services GmbH</a>, © <a href='//www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - Letecká ’18","tileUrl":"https://mapserver.mapy.cz/ophoto1618-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='//eox.at/' target='_blank'>EOX IT Services GmbH</a>, © <a href='//www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - Letecká ’15","tileUrl":"https://mapserver.mapy.cz/ophoto1415-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='//eox.at/' target='_blank'>EOX IT Services GmbH</a>, © <a href='//www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - Letecká ’12","tileUrl":"https://mapserver.mapy.cz/ophoto1012-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © GEODIS BRNO,s.r.o.","ignore":false},
  {"alt":"Mapy.cz - Letecká ’06","tileUrl":"https://mapserver.mapy.cz/ophoto0406-m/{z}-{x}-{y}","minZoom":2,"maxZoom":19,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='//eox.at/' target='_blank'>EOX IT Services GmbH</a>, © <a href='//www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - Letecká ’03","tileUrl":"https://mapserver.mapy.cz/ophoto0203-m/{z}-{x}-{y}","minZoom":2,"maxZoom":18,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='//eox.at/' target='_blank'>EOX IT Services GmbH</a>, © <a href='//www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a>","ignore":false},
  {"alt":"Mapy.cz - 2. voj. mapování","tileUrl":"https://mapserver.mapy.cz/army2-m/{z}-{x}-{y}","minZoom":5,"maxZoom":15,"attribution":"© 2nd Military Survey, Austrian State Archive, © MŽP ČR, © UJEP","ignore":false},
  {"alt":"Mapy.cz - Popisky","tileUrl":"https://mapserver.mapy.cz/hybrid-sparse-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>","overlay":true,"ignore":false},
  {"alt":"Mapy.cz - Popisky a cesty","tileUrl":"https://mapserver.mapy.cz/hybrid-plain-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>","overlay":true,"ignore":false},
  {"alt":"Mapy.cz - Popisky a trasy","tileUrl":"https://mapserver.mapy.cz/hybrid-trail_bike-m/{z}-{x}-{y}","minZoom":2,"maxZoom":20,"attribution":"© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>","overlay":true,"ignore":false},

  {"alt":"Google - Terenní","tileUrl":"https://mt{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}","attribution":"Mapová data © 2017 Google","subdomains":"0123","tileSize":256,"maxZoom":22,"ignore":false},
  {"alt":"Google - Hybridní","tileUrl":"https://mt{s}.google.com/vt?lyrs=y&x={x}&y={y}&z={z}","attribution":"Snímky © 2017 GEODIS Brno, Mapová data © 2017 Google","subdomains":"0123","tileSize":256,"maxZoom":22,"ignore":false},
  {"alt":"Google - Pouze terén","tileUrl":"https://mt{s}.google.com/vt?lyrs=t&x={x}&y={y}&z={z}","attribution":"© <a href='http://maps.google.com/'>Google</a>","subdomains":"0123","tileSize":256,"maxZoom":22,"ignore":false},
  {"alt":"Google - Popisky","tileUrl":"https://mt{s}.google.com/vt/lyrs=h&hl=cs&src=app&x={x}&y={y}&z={z}&s=","overlay":true,"attribution":"© <a href='http://maps.google.com/'>Google</a>","subdomains":"0123","ignore":false},
  {"alt":"Google - Popisky a provoz","tileUrl":"https://mt{s}.google.com/vt/lyrs=h,traffic&hl=cs&src=app&x={x}&y={y}&z={z}&s=","overlay":true,"attribution":"© <a href='http://maps.google.com/'>Google</a>","subdomains":"0123","ignore":false},

  {"alt":"MTB mapa Evropy","tileUrl":"https://tile.mtbmap.cz/mtbmap_tiles/{z}/{x}/{y}.png","minZoom":6,"maxZoom":18,"attribution":"© Martin Tesař, OpenStreetMap a USGS"},

  {"alt":"Freemap Turistická (SK, zastaralá)", "tileUrl":"https://tile.freemap.sk/T/{z}/{x}/{y}.jpeg","attribution":"Map &copy; <a href='http://www.freemap.sk/'>Freemap Slovakia</a>, data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors","minZoom":8,"maxZoom":16,"ignore":true },
  {"alt":"Freemap Cyklistická (SK, zastaralá)", "tileUrl":"https://tile.freemap.sk/C/{z}/{x}/{y}.jpeg","attribution":"Map &copy; <a href='http://www.freemap.sk/'>Freemap Slovakia</a>, data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors","subdomains":"1234","minZoom":8,"maxZoom":16,"ignore":true },
  {"alt":"Freemap Ortophotomozaika (SK)","tileUrl":"https://ofmozaika.tiles.freemap.sk/{z}/{x}/{y}.jpg","attribution":"Map &copy; <a href='http://www.freemap.sk/'>Freemap Slovakia</a>, data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors","minZoom":1,"maxZoom":20,"ignore":true },

  {"alt":"ArcGis - Topografická","tileUrl":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}.png","attribution":"© Esri","subdomains":"abc","ignore":false},
  {"alt":"ArcGis - Satelitní","tileUrl":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}.png","attribution":"© Esri","subdomains":"abc","maxZoom":17,"ignore":false},

  {"alt":"ČUZK - 1:25000 (CZ, zoom 11 - 15)","tileUrl":"https://geoportal.cuzk.cz/WMS_ZM25_PUB/WMService.aspx","crs":"EPSG:4326","layers":"GR_ZM25","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","overlay":false,"ignore":false,"minZoom":11,"maxZoom":15},
  {"alt":"ČUZK - 1:10000 (CZ, zoom 15 - 17)","tileUrl":"https://geoportal.cuzk.cz/WMS_ZM10_PUB/WMService.aspx","crs":"EPSG:4326","layers":"GR_ZM10","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","overlay":false,"ignore":false,"minZoom":15,"maxZoom":17},
  {"alt":"ČUZK - Satelitní (CZ)","tileUrl":"https://geoportal.cuzk.cz/WMS_ORTOFOTO_PUB/WMService.aspx","crs":"EPSG:4326","layers":"GR_ORTFOTORGB","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","overlay":false,"ignore":false,"minZoom":10,"maxZoom":20},
  {"alt":"ČUZK - Topografická (CZ)","tileUrl":"https://ags.cuzk.cz/arcgis/services/zmwm/MapServer/WMSServer","crs":"EPSG:3857","layers":"0","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","overlay":false,"ignore":false,"minZoom":10,"maxZoom":19},
  {"alt":"ČUZK - Jen terén (CZ)","tileUrl":"https://ags.cuzk.cz/arcgis2/services/dmr5g/ImageServer/WMSServer","crs":"EPSG:4326","layers":"dmr5g:GrayscaleHillshade","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","overlay":false,"ignore":false,"minZoom":10,"maxZoom":20},
  {"alt":"ČUZK - Stínování (CZ, zoom 10+)","tileUrl":"https://ags.cuzk.cz/arcgis2/services/dmr5g/ImageServer/WMSServer","crs":"EPSG:4326","layers":"dmr5g:GrayscaleHillshade","format":"image/png","transparent":false,"attribution":"© Český úřad zeměměřický a katastrální","opacity":"0.5","overlay":true,"ignore":false,"minZoom":10,"maxZoom":20},

  {"alt":"CENIA - II. voj. mapování (CZ)","tileUrl":"https://geoportal.gov.cz/ArcGIS/services/CENIA/cenia_rt_II_vojenske_mapovani/MapServer/WMSServer","crs":"EPSG:4326","layers":"0","format":"image/jpeg","transparent":false,"attribution":"© CENIA","overlay":false,"ignore":false,"minZoom":9,"maxZoom":16},
  {"alt":"CENIA - III. voj. mapování (CZ)","tileUrl":"https://geoportal.gov.cz/ArcGIS/services/CENIA/cenia_rt_III_vojenske_mapovani/MapServer/WMSServer","crs":"EPSG:4326","layers":"0","format":"image/jpeg","transparent":false,"attribution":"© CENIA","overlay":false,"ignore":false,"minZoom":9,"maxZoom":16},

  {"alt":"🗝️ Mapilion Hillshading (max zoom 12)","tileUrl":"https://mapilion-vector-and-raster-map-tiles.p.rapidapi.com/rapid-api/hillshades/v2/{z}/{x}/{y}","attribution":"<a href=\"https://mapilion.com/attribution\" target=\"_blank\">© Mapilion</a> <a href=\"http://www.openstreetmap.org/about/\" target=\"_blank\">© OpenStreetMap contributors</a><a href=\"http://www.openmaptiles.org/\" target=\"_blank\">© OpenMapTiles</a>","opacity":"0.5","overlay":true,"ignore":true,"minZoom":2,"maxZoom":12,"apiKey":"Mapilion Hillshading","apiKeyQuery":"rapidapi-key={apikey}"},
  {"alt":"🗝️ Maptiler Hillshading (max zoom 12)","tileUrl":"https://api.maptiler.com/tiles/hillshade/{z}/{x}/{y}.webp","attribution":"<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">© MapTiler</a> <a href=\"http://www.openstreetmap.org/about/\" target=\"_blank\">© OpenStreetMap contributors</a>","opacity":"0.5","overlay":true,"ignore":true,"minZoom":2,"maxZoom":20,"apiKey":"MapTiler Hillshading","apiKeyQuery":"key={apikey}"}
]
```

V mém forku jsou defaultně nainstalovány tyto mapy:

```json
[
  { "alt": "OpenStreetMap", "tileUrl": "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", "name": "osm", "subdomains": "abc" },
  { "alt": "🗝️ OpenCycleMap", "tileUrl": "https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png", "name": "ocm", "apiKey": "OpenCycleMap", "apiKeyQuery": "apikey={apikey}" },
  { "alt": "Bing Maps", "tileUrl": "https://ecn.t{s}.tiles.virtualearth.net/tiles/r{q}?g=864&mkt=en-gb&lbl=l1&stl=h&shading=hill&n=z", "subdomains": "0123", "minZoom": 1, "maxZoom": 20, "attribution": "<a href='https://www.bing.com/maps/'>Bing</a> map data copyright Microsoft and its suppliers", "name": "bingmap", "ignore": true },
  { "alt": "Bing Aerial View", "tileUrl": "https://ecn.t{s}.tiles.virtualearth.net/tiles/a{q}?g=737&n=z", "subdomains": "0123", "minZoom": 1, "maxZoom": 20, "attribution": "<a href='https://www.bing.com/maps/'>Bing</a> map data copyright Microsoft and its suppliers", "name": "bingaerial" },
  { "alt": "Google Maps", "tileUrl": "https://mt.google.com/vt?&x={x}&y={y}&z={z}", "name": "googlemaps", "attribution": "<a href='https://maps.google.com/'>Google</a> Maps", "subdomains": "1234", "tileSize": 256, "maxZoom": 22 },
  { "alt": "Google Satellite", "tileUrl": "https://mt.google.com/vt?lyrs=s&x={x}&y={y}&z={z}", "name": "googlemapssat", "attribution": "<a href='https://maps.google.com/'>Google</a> Maps Satellite", "subdomains": "1234", "tileSize": 256, "maxZoom": 22 },
  { "alt": "Freemap Outdoor (CZ, SK, jih Evropy)", "tileUrl": "https://outdoor.tiles.freemap.sk/{z}/{x}/{y}", "attribution": "Map &copy; <a href='http://www.freemap.sk/'>Freemap Slovakia</a>, data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors", "minZoom": 6, "maxZoom": 19 },
  { "alt": "Mapy.cz - Turistická", "tileUrl": "https://mapserver.mapy.cz/turist-m/{z}-{x}-{y}", "minZoom": 5, "maxZoom": 19, "attribution": "© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA", "ignore": true },
  {"alt": "ČUZK Hillshading (CZ, zoom 10+)", "tileUrl": "https://ags.cuzk.cz/arcgis2/services/dmr5g/ImageServer/WMSServer", "crs": "EPSG:4326", "layers": "dmr5g:GrayscaleHillshade", "format": "image/png", "transparent": false, "attribution": "© Český úřad zeměměřický a katastrální", "opacity": "0.5", "overlay": true, "ignore": false, "minZoom": 10, "maxZoom": 20 },
  {"alt": "ArcGis Hillshading (max zoom 16)", "tileUrl": "https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/tile/{z}/{y}/{x}", "attribution": "Esri | Esri, HERE, Garmin, METI/NASA, USGS", "opacity": "0.5", "overlay": true, "ignore": false, "minZoom": 2, "maxZoom": 16 }
]
```

Pokud nemáte můj fork, tak si tam přidejte ještě turistické mapy.cz

```json
[
  { "alt": "Mapy.cz - Turistická", "tileUrl": "https://mapserver.mapy.cz/turist-m/{z}-{x}-{y}", "minZoom": 5, "maxZoom": 19, "attribution": "© <a href='//www.seznam.cz' target='_blank'>Seznam.cz, a.s.</a>, © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, © NASA", "ignore": true },
  { "alt": "Freemap Outdoor (CZ, SK, Jih evropy)", "tileUrl": "https://outdoor.tiles.freemap.sk/{z}/{x}/{y}", "attribution": "Map &copy; <a href='http://www.freemap.sk/'>Freemap Slovakia</a>, data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors", "minZoom": 6, "maxZoom": 19},
  ]
```

Nové mapové podklady lze vytvořit za použití této šablony

```json
{
  "alt":"Readable Name",
  "tileUrl": "URL template including {s} (subdomain) and either {q} (quadkey) or {x},{y},{z} (Google/TMS tile coordinates + zoom)",
  "subdomains": "0123",
  "minZoom": 0,
  "maxZoom": 24,
  "attribution": "Copyright message (HTML allowed)",
  "name": "shortname",
  "tileSize": 256,
  "overlay": true,
  "apiKey": "name for key",
  "apiKeyQuery": "apikey={apikey}",
  "ignore": true
}
```

kde:

- **alt**: hlavní jméno mapy, **povinné**
- **tileUrl**: adresa k mapovým dlaždicím, lze použít *{s}* pro subdoménu, *{x}* a *{y}* pro souřadný systém dlaždice a *{z}* pro zoom, **povinné**
- **subdomains**: názvy subdomén
- **minZoom**: minimální zoom dlaždic
- **maxZoom**: maximální zoom dlaždic
- **attribution**: info o copyrightu
- **name**: zkrácené jméno
- **tileSize**: velikost dlaždice (zatím se nevyužívá)
- **overlay**: jde o překryvnou vrstvu
- **apiKey**: jméno políčka pro API key v administraci
- **apiKeyQuery**: řetězec, který se přidá do tileUrl, *{apikey}* se nahradí zadaným klíčem
- **ignore**: mapový podklad nebude zapnutý

#### Geocache Circles
- [originál](https://openuserjs.org/scripts/JRI/Geocache_Circles)
- [můj fork](https://openuserjs.org/scripts/Vylda/Geocache_Circles)

#### Mystery Wizard
- [webovka](https://www.saarfuchs.com/?s=mystery+wizard)
- [download](https://www.saarfuchs.com/wp-content/uploads/files/mystery-wizard.user.js)

#### Geocaching.com + Project-GC
- [webovka](https://project-gc.com/Home/UserScript)

#### GC little helper II
- [webovka](https://github.com/2Abendsegler/GClh)
- [download](https://github.com/2Abendsegler/GClh/raw/master/gc_little_helper_II.user.js)

#### Mapy.cz anywhere
- [webovka](http://www.gcgpx.cz/mapyczanywhere/)
- [download](http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.user.js)

#### CWG grabber
- [webovka](https://gitlab.com/Vylda/cwg-grabber#jak-spustit-cwg-grabber)
- [download](http://gcgpx.cz/cwggrabber/cwggrabber.user.js)

#### Clickable Areas
- [webovka](http://gcgpx.cz/clickablaearea/)
- [download](http://www.gcgpx.cz/clickablaearea/clickablaearea.user.js)

#### AngleMeter
- [webovka](http://www.gcgpx.cz/anglemeter/)
- [download](http://www.gcgpx.cz/anglemeter/anglemeter.user.js)

#### Geocaching Personal notes & Dashboard Events Marker
- [webovka](http://gcgpx.cz/dashboardevents/)
- [download](http://www.gcgpx.cz/dashboardevents/dashboardevents.user.js)

#### Show Personal Notes filter
- [webovka](http://www.gcgpx.cz/fpn/)
- [download](http://www.gcgpx.cz/fpn/filter-personal-notes.user.js)

#### Geocaching Events: add to calendar
- [webovka](http://www.gcgpx.cz/eventscalendar/)
- [download](http://www.gcgpx.cz/eventscalendar/event-calendar-links.user.js)

#### Automagic Jigidi Solver
- [webovka a download] (<https://greasyfork.org/cs/scripts/394279-automagic-jigidi-solver>)

Tento solver je blokován na Jigidi stránkách.

#### Jigidi Magic Stripes
- řeší Jigidi puzzle po sloupcích
- [webovka a download](https://gist.github.com/Dan-Q/b5e4dbb45851b07042b6a57ebe1005a7)
